from django.contrib import admin
from django.urls import path, re_path, include
import yarrow3.groggy.urls

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'', include(yarrow3.groggy.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
