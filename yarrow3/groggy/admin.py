from django.contrib import admin
import yarrow3.groggy.models as groggy_models

class EntryInline(admin.TabularInline):
    model = groggy_models.Entry

@admin.register(groggy_models.Item)
class ItemAdmin(admin.ModelAdmin):
    inlines = [
            EntryInline,
            ]
