from django.shortcuts import render
from django.views import View
import yarrow3.groggy.models as groggy_models
from django.shortcuts import get_object_or_404

import logging
logger = logging.getLogger(name='yarrow3')

class RootPage(View):

    def get(self, request, *args, **kwargs):

        logger.info("Serving root page")

        result = render(
                request=request,
                template_name='root-page.html',
                context = {
                    },
                )

        return result

class ItemPage(View):

    def get(self, request,
            itemid,
            *args, **kwargs):

        logger.info("Serving item page %s",
                itemid)

        this_item = get_object_or_404(groggy_models.Item, slug=itemid)

        entries = this_item.entries

        result = render(
                request=request,
                template_name='item.html',
                context = {
                    'itemid': itemid,
                    'entries': entries,
                    },
                )

        return result


