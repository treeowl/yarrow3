import yarrow3.groggy.models as groggy_models

import logging
logger = logging.getLogger(name="yarrow3")

def post(
        content,
        poster,
        parent = None,
        ):

    logger.info("User %s is posting to %s",
            poster, parent)
    logger.debug("  -- text is %s",
            content)

    def split_title(text):
        try:
            title, body = text.split('\n', 1)
        except ValueError:
            title = text
            body = text

        return title, body

    if parent is None:
        title, body = split_title(content)
    else:
        title = None
        body = content

    body = body.strip()
    if not body:
        logger.info("  -- rejected; no body")
        raise ValueError("No body")

    logger.info("Posting title %s, parent %s",
            title,
            parent)
    logger.debug("  -- with body:\n%s\n== body ends ==",
            body)

    if parent is None:
        item = groggy_models.Item(
                title = title,
                )
        item.save()

        logger.info("  -- created new item: %s",
                item)

    else:
        item = parent

        # Has it already gone into a continuation?

        seen = {}
        while item not in seen:

            seen[item] = True

            try:
                item = groggy_models.Item.objects.get(
                        previous = item,
                        )
                logger.debug('  -- this is a continuation to %s',
                        item)
            except groggy_models.Item.DoesNotExist:
                # great, we've found the end of the chain
                logger.debug('  -- end of continuations')
                break
        else:
            # should never happen, but just in case
            logger.warning('  -- loop in item continuations! %s',
                    list(seen.keys()))
            raise ValueError("Loop in item continuations")

        # Right, we have an item we could probably reply to--
        # unless it's full.

        if item.is_full:
            title, body = split_title(body)

            new_item = groggy_models.Item(
                    previous = item,
                    title = title,
                    )
            new_item.save()

            logger.info('  -- %s continued in item %s',
                    item, new_item)

            item = new_item

    entry = groggy_models.Entry(
            item = item,
            content = body,
            poster = poster,
        )
    entry.save()

    logger.debug("  -- posted entry: %s",
            entry)

    return item
