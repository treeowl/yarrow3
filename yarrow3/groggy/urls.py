from django.urls import path, re_path
import django.contrib.auth.views
import yarrow3.groggy.views as groggy_views

urlpatterns = [
        path('',
            groggy_views.RootPage.as_view()),

        re_path(r'^(?P<itemid>[A-Z][0-9]{7})$',
            groggy_views.ItemPage.as_view()),

        ]
