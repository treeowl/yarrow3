from django.apps import AppConfig

class GroggyConfig(AppConfig):
    name = 'yarrow3.groggy'
