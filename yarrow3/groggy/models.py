from django.db import models
import os
from django.conf import settings
from django.db import transaction
import datetime
import django.db.utils

import logging
logger = logging.getLogger(name="yarrow3")

class Entry(models.Model):

    id = models.AutoField(primary_key=True)

    grogname = models.CharField(
            max_length = 255,
            )

    poster = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            on_delete = models.DO_NOTHING,
            )

    content = models.TextField()

    date = models.DateTimeField(
            auto_now_add=True,
            blank=True,
            )

    item = models.ForeignKey(
            'Item',
            on_delete = models.CASCADE,
            )

    def __str__(self):
        result = f'[{self.id} - {self.poster} on {self.item}]'

        return result

class Item(models.Model):

    id = models.AutoField(
            primary_key = True,
            )

    slug = models.SlugField(
            max_length = 8,
            unique = True,
            )

    title = models.CharField(
            max_length = 256,
            )

    previous = models.ForeignKey(
            'self',
            related_name = 'next_one',
            on_delete = models.DO_NOTHING,
            null = True,
            blank = True,
            )

    class FullException(Exception):
        pass

    def save(self, *args, **kwargs):

        if self.slug:
            return super().save(*args, **kwargs)

        now = datetime.datetime.utcnow()
        year_letter = chr(97+now.year%26)

        serial = \
                now.timetuple().tm_yday * 10000 + \
                now.hour * 100 + \
                now.minute

        while True:

            self.slug = '%s%07d' % (year_letter, serial)

            try:

                with transaction.atomic():
                    return super().save(*args, **kwargs)

            except django.db.IntegrityError:
                serial += 1
                # round again, then

    @property
    def next(self):
        return self.next_one.first()

    def __str__(self):
        if self.slug:
            return self.slug

        return super().__str__()

    @property
    def is_full(self):
        count = Entry.objects.filter(item=self).count()

        if count>=settings.YARROW3_MAX_ITEM_LENGTH:
            logger.debug("%s: full; contains %d entries (max is %d)",
                    self, count,
                    settings.YARROW3_MAX_ITEM_LENGTH,
                    )
            return True

        return False

    @property
    def entries(self):
        result = list(Entry.objects.filter(
                item = self,
                ).order_by(
                        'date',
                        ))

        return result

    def count(self):
        return Entry.objects.filter(
                item = self,
                ).count()

    def continuation(self):
        """
        Creates a new, empty Item, and sets this Item to
        be its "previous". If there's already such an Item,
        returns that Item.
        """

        try:
            existing = Item.objects.get(
                    previous = self,
                    )
            return existing
        except Item.DoesNotExist:
            pass

        newbie = Item(
            previous = self,
            )
        newbie.save()

        return newbie
